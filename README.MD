# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Aleksey Krivotulov

* **E-MAIL**: akrivotulov@tsconsulting.com

## SOFTWARE

* OPENJDK 8

* Intellij Idea

* Windows 10 Pro

## HARDWARE

* **RAM**: 16GB

* **CPU**: Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz   2.59 GHz

* **SSD**: 512GB

## BUILD APPLICATION

```bash
mvn clean install
```

## RUN PROGRAM

```bash
java -jar ./task-manager.jar
```
