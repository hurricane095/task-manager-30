package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;

/**
 * IAuthService
 *
 * @author Aleksey_Krivotulov
 */
public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @Nullable
    User registry(@Nullable String login,
                  @Nullable String password,
                  @Nullable String email);

    @NotNull
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role[] roles);
}
