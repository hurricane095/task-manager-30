package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;

/**
 * IUserService
 *
 * @author Aleksey_Krivotulov
 */
public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @NotNull Role role);

    @Nullable
    User deleteByLogin(@Nullable String login);

    @Nullable
    User deleteByEmail(@Nullable String email);

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    User updateUser(@Nullable String userId,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
