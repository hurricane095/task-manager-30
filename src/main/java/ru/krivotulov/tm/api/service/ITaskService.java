package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name,
                @Nullable String description,
                @Nullable Date dateBegin,
                @Nullable Date dateEnd);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description);

    @NotNull
    Task updateByIndex(@Nullable String userId,
                       @Nullable Integer index,
                       @Nullable String name,
                       @Nullable String description);

    @NotNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
