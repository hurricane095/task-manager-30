package ru.krivotulov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
