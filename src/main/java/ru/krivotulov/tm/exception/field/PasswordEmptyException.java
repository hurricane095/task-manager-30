package ru.krivotulov.tm.exception.field;

/**
 * LoginEmptyException
 *
 * @author Aleksey_Krivotulov
 */
public final class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! password is empty...");
    }

}
