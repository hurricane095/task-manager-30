package ru.krivotulov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.command.data.AbstractDataCommand;
import ru.krivotulov.tm.command.data.DataBackupLoadCommand;
import ru.krivotulov.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Backup
 *
 * @author Aleksey_Krivotulov
 */
public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop(){
        executorService.shutdown();
    }

    public void save() {
        bootstrap.runCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.runCommand(DataBackupLoadCommand.NAME, false);
    }

}
