package ru.krivotulov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.model.ICommand;
import ru.krivotulov.tm.api.service.IServiceLocator;
import ru.krivotulov.tm.enumerated.Role;

/**
 * AbstractCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractCommand implements ICommand {

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract Role[] getRoles();

    public abstract void execute();

    @Getter
    @Setter
    @NotNull
    protected IServiceLocator serviceLocator;

    @Override
    public String toString() {
        @NotNull final String name = getName();
        @NotNull final String argument = getArgument();
        @NotNull final String description = getDescription();
        return name + " : " + argument + " : " + description;
    }

}
