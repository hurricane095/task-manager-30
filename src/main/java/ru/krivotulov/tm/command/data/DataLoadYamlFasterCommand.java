package ru.krivotulov.tm.command.data;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * DataLoadYamlFasterCommand
 *
 * @author Aleksey_Krivotulov
 */
public class DataLoadYamlFasterCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml-faster";

    @NotNull
    public static final String DESCRIPTION = "Load yaml faster.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML FASTER LOAD]");
        @NotNull byte[] bytesJson = Files.readAllBytes(Paths.get(FILE_FASTER_YAML));
        @NotNull final String json = new String(bytesJson);
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
        @NotNull final Domain domain = yamlMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

}
