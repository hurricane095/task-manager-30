package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskShowByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}
